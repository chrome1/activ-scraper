const capitalize = function (s) {
  return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
};

const getUnique = function (arr) {
  return arr.reduce((acc, current) => {
    const itemFound = acc.find(
      (item) =>
        item.name.toLowerCase() === current.name.toLowerCase() &&
        item.phone === current.phone
    );
    return itemFound ? acc : acc.concat([current]);
  }, []);
};

const getActive = function (arr) {
  return arr.filter((item) => item.isOn);
};

const scrapeOncalls = function () {
  const oncallForms = ["#form-p2", "#form-p3"];
  let oncallUsers = [];

  const extractOncalls = (formId) => {
    if (!document.querySelector(formId)) {
      console.log("NO FORM FOUND HAVING ID: " + formId);
      return;
    }

    let form = document.querySelectorAll(`${formId} tbody tr`);

    form.forEach(function (tr, index) {
      let skipToNext = false;

      let oncall = {
        isOn: null,
        phone: null,
        phoneType: null,
        name: null,
        index: null,
      };

      Array.from(tr.cells).forEach(function (cell, index) {
        // skip first cell
        if (index === 0) return;
        // phone
        if (index === 1) {
          if (cell.children[0].value === "123-456-7890") {
            skipToNext = true;
            return;
          }
          oncall.phone = cell.children[0].value;
        }
        // name
        if (index === 2 && !skipToNext) {
          oncall.name = cell.children[0].value;
        }

        // on/off
        if (index === 3 && !skipToNext) {
          oncall.isOn = Boolean(parseInt(cell.children[0].value));
        }

        // phone type
        if (index === 4 && !skipToNext) {
          oncall.phoneType = cell.children[0].value;
        }
      });

      if (formId === "#form-p2" && !skipToNext) {
        oncall.index = `p2-${index}`;
        oncallUsers.push(oncall);
      }

      if (formId === "#form-p3" && !skipToNext) {
        oncall.index = `p3-${index}`;
        oncallUsers.push(oncall);
      }
    });
  };

  oncallForms.forEach(extractOncalls);

  oncallUsers = getUnique(oncallUsers);

  return {
    p2: oncallUsers.filter((user) => user.index.split("-")[0] === "p2"),
    p3: oncallUsers.filter((user) => user.index.split("-")[0] === "p3"),
  };
};

const deleteSingleOncallUser = function (oncallKey, sendResponse) {
  chrome.storage.local.get(["oncallScrapedData"], (result) => {
    let oldOncallUserData = result.oncallScrapedData;
    let newOncallUserData = {};
    if (oncallKey.split("-")[0] == "p2") {
      let p2Users = oldOncallUserData.p2;
      p2Users = p2Users.filter((user) => user.index !== oncallKey);
      newOncallUserData = { ...oldOncallUserData, p2: p2Users };
    } else {
      let p3Users = oldOncallUserData.p3;
      p3Users = p3Users.filter((user) => user.index !== oncallKey);
      newOncallUserData = { ...oldOncallUserData, p3: p3Users };
    }

    // set new user data in storage
    chrome.storage.local.set(
      { oncallScrapedData: newOncallUserData },
      () => {}
    );

    sendResponse({
      message: "new_oncall_data_successfuly_scraped",
      data: newOncallUserData,
    });
  });
};

const fetchAllFromStorage = function (sendResponse) {
  try {
    chrome.storage.local.get(["oncallScrapedData"], (result) => {
      sendResponse({
        message: "fetch_all_from_storage_successfully",
        data: result.oncallScrapedData,
      });
    });
  } catch (error) {
    // should be a failure response instead
    sendResponse({
      message: "fetch_all_from_storage_successfully",
      data: null,
    });
  }
};

const createMaintenanceUsers = function (oncallUsers) {
  oncallUsers.p2.forEach(function (user, index) {
    let firstNameField = document.querySelector(
      `input[name='users[${index}][first_name]']`
    );
    let lastNameField = document.querySelector(
      `input[name='users[${index}][last_name]']`
    );
    let phoneField = document.querySelector(
      `input[name='users[${index}][phone_primary]']`
    );
    let phoneTypeField = document.querySelector(
      `select[name='users[${index}][phone_primary_type_id]']`
    );
    if (firstNameField) {
      let firstName = user.name.split(" ")[0] || "Oncall";
      firstName = capitalize(firstName);
      if (firstName === "On") {
        // handles "On Call Phone"
        firstName = "Oncall";
      }
      firstNameField.value = firstName;
    }
    if (lastNameField) {
      let lastName = user.name.split(" ")[1] || "Oncall";
      lastName = capitalize(lastName);
      if (lastName === "Call") {
        // handles "On Call Phone"
        lastName = "Phone";
      }
      lastNameField.value = lastName;
    }
    if (phoneField) {
      phoneField.value = user.phone;
    }
    if (phoneTypeField) {
      switch (user.phoneType) {
        case "Pager":
          phoneTypeField.selectedIndex = "2";
          break;
        case "Direct":
          phoneTypeField.selectedIndex = "3";
          break;
        default:
          phoneTypeField.selectedIndex = "1";
      }
    }
  });
};

const createCourtesyUsers = function (oncallUsers) {
  oncallUsers.p3.forEach(function (user, index) {
    let newIndex = index + oncallUsers.p2.length;
    let firstNameField = document.querySelector(
      `input[name='users[${newIndex}][first_name]']`
    );
    let lastNameField = document.querySelector(
      `input[name='users[${newIndex}][last_name]']`
    );
    let phoneField = document.querySelector(
      `input[name='users[${newIndex}][phone_primary]']`
    );
    let phoneTypeField = document.querySelector(
      `select[name='users[${newIndex}][phone_primary_type_id]']`
    );
    if (firstNameField) {
      let firstName = user.name.split(" ")[0] || "Oncall";
      firstName = capitalize(firstName);
      if (firstName === "On") {
        // handles "On Call Phone"
        firstName = "Oncall";
      }
      firstNameField.value = firstName;
    }
    if (lastNameField) {
      let lastName = user.name.split(" ")[1] || "Oncall";
      lastName = capitalize(lastName);
      if (lastName === "Call") {
        // handles "On Call Phone"
        lastName = "Phone";
      }
      lastNameField.value = lastName;
    }
    if (phoneField) {
      phoneField.value = user.phone;
    }
    if (phoneTypeField) {
      switch (user.phoneType) {
        case "Pager":
          phoneTypeField.selectedIndex = "2";
          break;
        case "Direct":
          phoneTypeField.selectedIndex = "3";
          break;
        default:
          phoneTypeField.selectedIndex = "1";
      }
    }
  });
};

const transferOncalls = function (sendResponse) {
  // fetch all from storage
  chrome.storage.local.get(["oncallScrapedData"], (result) => {
    let oncallUsers = result.oncallScrapedData;
    createMaintenanceUsers(oncallUsers);
    createCourtesyUsers(oncallUsers);
    // finally clear storage
    chrome.storage.local.remove(["oncallScrapedData"], function () {
      sendResponse({
        message: "successfully_transfered_new_oncalls",
      });
    });
  });
};

// listen for messages
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  // oncall user delete button
  if (request.command == "delete_oncall_from_storage") {
    deleteSingleOncallUser(request.key, sendResponse);
  }

  // scrape oncall button clicked
  if (request.command == "scrape_oncalls") {
    // clear all data first
    chrome.storage.local.remove(["oncallScrapedData"], function () {
      if (chrome.runtime.lastError) {
        console.error(chrome.runtime.lastError);
      } else {
        // scrape after data is cleared
        let oncallUserData = scrapeOncalls();
        chrome.storage.local.set(
          { oncallScrapedData: oncallUserData },
          () => {}
        );
        sendResponse({
          message: "oncall_data_successfuly_scraped",
          data: oncallUserData, // may not need to send the data
        });
      }
    });
  }

  // fetch all from storage
  if (request.command == "fetch_all_from_storage") {
    fetchAllFromStorage(sendResponse);
  }

  // create new oncalls
  if (request.command == "create_new_oncalls") {
    transferOncalls(sendResponse);
  }

  return true;
});

// perform any clean up here
// chrome.runtime.onSuspend.addListener(function () {
//   console.log("Unloading.");
// });
