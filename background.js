// Extensions are event based programs used to modify or enhance the Chrome browsing experience.
// Events are browser triggers, such as navigating to a new page, removing a bookmark, or closing a tab.
// Extensions monitor these events in their background script, then react with specified instructions.
// A background page is loaded when it is needed, and unloaded when it goes idle. Some examples of events include:
// ---- The extension is first installed or updated to a new version.
// ---- The background page was listening for an event, and the event is dispatched.
// ---- A content script or other extension sends a message.
// ---- Another view in the extension, such as a popup, calls runtime.getBackgroundPage.

// The only occasion to keep a background script persistently active is if the extension uses chrome.webRequest API to block
// or modify network requests. The webRequest API is incompatible with non-persistent background pages.

// Listen to the runtime.onInstalled event to initialize an extension on installation.
// chrome.runtime.onInstalled.addListener(function () {
//   chrome.contextMenus.create({
//     id: "sampleContextMenu",
//     title: "Sample Context Menu",
//     contexts: ["selection"],
//   });
// });

// perform any clean up here
chrome.runtime.onSuspend.addListener(function () {
  console.log("Unloading.");
  //   chrome.browserAction.setBadgeText({ text: "" });
});
