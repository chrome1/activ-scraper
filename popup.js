const closePopupButton = document.querySelector("#close-popup");
const contentDiv = document.querySelector("#content");
const scrapeButton = document.querySelector("button#scrape");
const viewStorageButton = document.querySelector("button#view-storage");
const createOncallButton = document.querySelector("button#create-oncalls");

const stringToHTML = function (str) {
  var parser = new DOMParser();
  var doc = parser.parseFromString(str, "text/html");
  return doc.body;
};

const deleteOncallUser = function (e) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { command: "delete_oncall_from_storage", key: e.target.id },
      null,
      function (response) {
        if (chrome.runtime.lastError) {
          console.log(chrome.runtime.lastError);
        }
        if (response.message == "new_oncall_data_successfuly_scraped") {
          appendOncallUserHtml(response);
        }
      }
    );
  });
};

const Html = {
  openTable() {
    return `
        <table class="w-full bg-gray-200 text-gray-800">
        <tr class="text-left border-b-2 border-gray-300">
            <th class="px-4 py-3">Name</th>
            <th class="px-4 py-3">Phone</th>
            <th class="px-4 py-3">Phone Type</th>
            <th class="px-4 py-3"></th>
        </tr>
    `;
  },
  closeTable() {
    return `</table>`;
  },
  getTotal(total) {
    return `<p class="my-2 font-semibold">Total: ${total}</p>`;
  },
  getOncallRow(oncall) {
    return `<tr class="bg-gray-100 border-b border-gray-200">
       <td class="px-4 py-3">${oncall.name}</td>
       <td class="px-4 py-3">${oncall.phone}</td>
       <td class="px-4 py-3">${oncall.phoneType}</td>
       <td class="px-4 py-3">
        <i class="mt-2 fa fa-times-circle text-gray-300 hover:text-gray-600 transition duration-150 cursor-pointer oncall-user-item" id="${oncall.index}"></i>
       </td>
     </tr>`;
  },
};

const appendOncallUserHtml = function (response) {
  if (typeof response.data == "undefined") {
    contentDiv.innerHTML = "Storage is empty.";
    return;
  }

  var html = "";
  html += Html.getTotal(response.data.p2.length + response.data.p3.length);

  // append maintenance users
  if (response.data.p2.length > 0) {
    html += `
    <h6 class="text-xl text-left font-normal leading-normal mt-0 mb-2 text-gray-800">
        Maintenance
    </h6>
  `;

    html += Html.openTable();

    response.data.p2.forEach((oncall) => {
      html += Html.getOncallRow(oncall);
    });

    html += Html.closeTable();
  }

  // append courtesy users
  if (response.data.p3.length > 0) {
    html += `
    <h6 class='text-xl text-left font-normal leading-normal mt-0 mb-2 text-gray-800'>
        Courtesy
    </h6>
  `;

    html += Html.openTable();

    response.data.p3.forEach((oncall, index) => {
      html += Html.getOncallRow(oncall);
    });

    html += Html.closeTable();
  }

  let newHtml = stringToHTML(html);
  const buttons = newHtml.querySelectorAll(".oncall-user-item");
  for (const button of buttons) {
    button.addEventListener("click", deleteOncallUser);
  }
  contentDiv.innerHTML = "";
  contentDiv.appendChild(newHtml);
};

const scrapeBtnHandler = function () {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { command: "scrape_oncalls" },
      null,
      function (response) {
        if (chrome.runtime.lastError) {
          console.log(chrome.runtime.lastError);
        }

        if (response.message === "oncall_data_successfuly_scraped") {
          appendOncallUserHtml(response);
        }
      }
    );
  });
  return true;
};

const fetchAllFromStorage = function () {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { command: "fetch_all_from_storage" },
      null,
      function (response) {
        if (chrome.runtime.lastError) {
          console.log(chrome.runtime.lastError);
        }

        if (response.message === "fetch_all_from_storage_successfully") {
          appendOncallUserHtml(response);
        }
      }
    );
  });
  return true;
};

const transferOncalls = function (sendResponse) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { command: "create_new_oncalls" },
      null,
      function (response) {
        if (chrome.runtime.lastError) {
          console.log(chrome.runtime.lastError);
        }

        if (response.message === "successfully_transfered_new_oncalls") {
          contentDiv.innerHTML = "Users Transfered";
        }
      }
    );
  });
  return true;
};

// scrape button clicked
scrapeButton.addEventListener("click", scrapeBtnHandler);

// view storage clicked
viewStorageButton.addEventListener("click", fetchAllFromStorage);

// create new oncalls
createOncallButton.addEventListener("click", transferOncalls);

// close popup button clicked
closePopupButton.addEventListener("click", function () {
  window.close();
});
